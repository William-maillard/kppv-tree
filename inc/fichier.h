/***********************************************
 * Module de chargement des données du fichier *
 * dans un tableau de points  et de sauvegarde *
 * d'un tableau de points dans un fichier      *
 ***********************************************/
#ifndef _FICHIER_H
#define _FICHIER_H

#include "type.h"
#include "arbre_kd.h"

/* *** Charge les points dans un tableau de points et dans un arbre kd *** */
void chargement_fichier(char *s, tableau_de_points *t, arbre_kd *a);

/* *** Fonction de sauvegarde *** */
int sauvegarde_fichier(tableau_de_points t, char *chemin_fichier);

#endif
