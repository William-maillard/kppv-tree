/*********************************************************
 * Déclarations des fonctions permettant de traiter les  *
 * évènements du programme = clique sur les boutons.     *
 * Ce module a pour but d'aléger la fonction main, et    *
 * de rendre le traitement des évènements plus lissible  *
 *********************************************************/
#ifndef _EVENEMENT_H_
#define _EVENEMENT_H_

/* *** Traite les évènements spécifiques du mode création. *** */
void event_mode_creation(int x, int y, tableau_de_points *t, int *compteur, arbre_kd *a);

/* *** Taite les évènements en mode KPPV *** */
void event_mode_KPPV(int x, int y, int *k, point *point_clique, tableau_de_points t, liste_KPPV *l_KPPV, arbre_kd a);

#endif
