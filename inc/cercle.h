/**************************************************
 * Fonction pour tracer un cercle de voisinage.   *
 * Pour cela, on utilise l'équation d'un cercle : *
 *   (x-x0)^2 + (y-y0)^2 = rayon^2                *
 * On prend x comme inconnue secondaire.          *
 * On va incrémenter x de i à chaque tour de      *
 * boucle et déterminer y avec la formule.        *
 * Puis on dessinera le cerlce à l'aide de la     *
 * fonction MLV_draw_polygon().                   *
 **************************************************/
#ifndef _CERCLE_H_
#define _CERCLE_H_
#include <MLV/MLV_color.h>
#include "type.h"

/****
 * p0 : centre du cercle.
 * p : point appartenant au cercle.
 ****/
void dessiner_cercle(point p0, point p, MLV_Color c);

#endif
