/**************************************************************
 * Déclarations des fonctions implémantant le mode k-ppv :    *
 *   -créer la liste des voisins                              *
 *   -attribuer une classe au point en fonction de ces k-ppv  *
 *   -afficher le cercle de voisinage                         *
 *   -la mise en avant des k-ppv                              *
 **************************************************************/
#ifndef _KPPV_H
#define _KPPV_H

#include "listes.h"
#include "type.h"

/* *** Crée et renvoie la liste des voisins *** */
liste_KPPV creer_liste_voisins(tableau_de_points t, point pt);

/* *** attribut une classe à un point en fonction de ses k voisins *** */
void kppv_decision(liste_KPPV l, point *pt, int k);

/* *** met en avant les k premiers éléments d'une liste_KPPV retourne
   le pointeur sur le k° voisin (utilisé pour l'actualisation du plan)*** */
liste_KPPV mise_en_avant_k_ppv(int k, liste_KPPV l_KPPV);

/* *** supprime la mise en avant des k éléments de la liste *** */
void supprime_mise_en_avant(int k, liste_KPPV l);

#endif
