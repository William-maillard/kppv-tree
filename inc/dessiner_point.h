/*****************************************************
 * Déclarations des fonctions pour :                 *
 *   -définition de l'état dun point                 *
 *   -afficher un point selon sa classe et son état  *
 *   -afficher les points d'un tableau_de_points     * 
 *   -traiter un point cliqué                        *
 *    (création de la structure + affichage)         * 
 *****************************************************/
#ifndef _DESSINER_POINT_H_
#define _DESSINER_POINT_H_

#include <MLV/MLV_color.h>
#include "type.h"

/* *** macros pour définir l'état d'un point *** */
typedef enum{EFFACE, AFFICHE, EN_VALEUR} etat_point;

/* *** Fonction qui affiche le symbole d'un point suivant sa classe
   x et y sont les coordonnées entières du point. *** */
void affiche_point_classe(point pt, etat_point etat);

/* *** Fonction qui affiche tous les points d'un tableau_de_points *** */
void afficher_tableau_de_points(tableau_de_points t);

/* *** Fonction qui crée la structure point du point cliqué, et qui affiche ce point.
   La fonction retourne la structure point *** */
point affiche_point_clique(int x, int y);

#endif
