/*************************************
 * déclarations des fonctions pour : *
 *   -créer un liste vide            *
 *   -tester si une liste est vide   *
 *   -libérer la mémoire d'un liste  *
 *************************************/
#ifndef _LISTES_H
#define _LISTES_H

#include "type.h"

/* *** renvoie une liste vide *** */
liste_KPPV liste_vide();

/* *** teste si une liste_KPPV est vide *** */
int est_liste_vide(liste_KPPV l);

/* *** fonction qui libère l'espace mémoire occupé par la liste
   et met la liste à liste_vide *** */
void detruire_liste(liste_KPPV *l);

/* *** Fonction de calcul de la distance euclidienne, utilisé pour trier
  la liste *** */
float distance_euclidienne(point *pt1, point *pt2);

/* ***
 * Fonction d'insertion d'un voisin pt (dont les champs on déjà étés remplis)
 * dans une liste triée par ordre de distance euclidienne donné en paramètre
 *** */
typedef enum{CROISSANT, DECROISSANT} tri;
liste_KPPV insere_liste_trie(liste_KPPV l, voisin *pt, tri tri);

#endif
