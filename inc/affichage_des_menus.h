/******************************************************
 * Déclarations des fonctions pour :                  *
 *   -attribuer les dimensions à chaque bouton        *
 *   -gérer l'affichage des options pour chaque mode  *
 *   -afficher une boite de saisie sur un bouton      *
 ******************************************************/
#ifndef _AFFICHAGE_DES_MENUS_H_
#define _AFFICHAGE_DES_MENUS_H_

#include "bouton.h"
#include "type.h"

/****
 * affiche/efface le cadre et le texte "options affichage"
 ****/
void afficher_cadre_options_affichage(MLV_Color c);

/****
 * affiche et affecte les dimensions des boutons déclarés dans bouton.h,
 * il doivent être créé avant
 *** */
void afficher_les_boutons_clickables();

/* *** Permet de passer du mode 'current_mode' au mode 'new_mode' *** */
void change_mode(bouton new_mode, bouton current_mode);

/* *** Change la structure utilisée pour la recherche des k-ppv. *** */
void change_structure(bouton new_structure, bouton current_structure);

/****
 * affiche une input box sur le bouton en paramètre
 * et renvoie le text rentré par l'utilisateur
 ****/
char *demande_valeur(bouton b);

#endif
