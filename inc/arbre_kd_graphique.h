/***********************************************************
 * Contient les options d'affichage proposés en mode k-ppv *
 * pour l'arbre kd.                                        *
 ***********************************************************/
 #ifndef _ARBRE_KD_GRAPHIQUE_H_
 #define _ARBRE_KD_GRAPHIQUE_H_

#include <MLV/MLV_color.h>
#include "../inc/arbre_kd.h"

/* *** Dessine la découpe du plan faite par l'arbre a *** */
void decoupe_plan(arbre_kd a, MLV_Color c);

/* *** animation de la recherche, dessine la zone courante. *** */
void annimation(zone z, MLV_Color c, int ms);

#endif
