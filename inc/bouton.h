/*****************************************************
 * Fichier qui contient les déclarations suivantes   *
 * -type bouton                                      *
 * -variables globales de type bouton                *
 * -fonctions de manipulation de ce type             *
 *****************************************************/
#ifndef BOUTON_H
#define BOUTON_H

/* ***** DEFINITION DU TYPE BOUTON ***** */
typedef enum
{
  INVISIBLE,
  VISIBLE,
  ACTIF,
  QUITTER
} mode;

struct struct_bouton
{
  int x, y, width, height;
  char *name;
  mode etat;
};
typedef struct struct_bouton *bouton;

/*_______________________________________*/

/* ***** LES BOUTONS DU PROGRAMME ***** */
/*--boutons divers--*/
extern bouton b_creation, b_KPPV, b_reinitialisation, b_quitter, b_effacer;
/*--boutons pour les inputs--*/
extern bouton b_sauvegarde, b_valeur_k, b_classe, b_chargement;
/*--boutons pour les options d'affichage--*/
extern bouton b_voisinage, b_decision, b_decoupe_plan, b_animation;
/*--boutons pour le type de structure utilisée--*/
extern bouton b_tableau, b_arbre;
/*permet d'utiliser la fct est_clique() pour savoir quand on clique dans le plan*/
extern bouton b_plan;
/*_______________________________________*/

/* *** crée un bouton "name", sans initialiser ces dimensions *** */
bouton creer_bouton(char *name);

/* *** Créer les boutons du programme. ***/
void creer_les_boutons();

/* ***
 * Fonction qui affiche un bouton dont les champs ont tous étés initialisés
 * la couleur dépend du champ etat
 *** */
void afficher_bouton(bouton b);

/* *** Fonction qui renvoie 1 si les coordonnées (x,y) sont situe dans le bouton,
0 sinon *** */
int est_clique(int x, int y, bouton b);

/* *** Fonction qui libère la mémoire occupé par un bouton *** */
void supprimer_bouton(bouton *b);

/* *** libère la mémoire occupé par tous les boutons du programme *** */
void detruire_les_boutons();

#endif
