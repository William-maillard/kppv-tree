/********************************************************************
 * Macros permettant de délimiter le plan, de faire la graduation,  *
 * de définir une échelle.                                          *
 * Déclarations des fonctions pour :                                *
 *   -afficher un plan                                              *
 *   -aficher les coordonnées de la souris au survol du plan        *
 *   -effacer et afficher un nouveau plan                           *
 ********************************************************************/
#ifndef _DESSINE_PLAN_H_
#define _DESSINE_PLAN_H_

/* *** dimensions de la fenêtre *** */
#define WIDTH 1200
#define HEIGHT 700

/* *** Macro pour les tailles limites du plan et la graduation *** */
/*--à modifier si l'on veut changer l'échelle--*/
#define PADDING 10 /* espace entre le plan et le cadre*/
#define LONGUEUR_AXE_Y HEIGHT - 2 * PADDING /* plan sur tout la hauteur*/
#define LONGUEUR_AXE_X LONGUEUR_AXE_Y /*pour avoir un plan normé*/
#define VALEUR_MIN_X "-1"
#define VALEUR_MAX_X "1"
#define VALEUR_MIN_Y "-1"
#define VALEUR_MAX_Y "1"
#define NB_GRADUATIONS 20
#define GAUCHE_PLAN PADDING /*le cadre sera coller au bord*/
#define HAUT_PLAN PADDING /* idem */


/*--ne pas changer, macros utilisées pour placer les points--*/
#define DROITE_PLAN GAUCHE_PLAN + LONGUEUR_AXE_X
#define BAS_PLAN HAUT_PLAN + LONGUEUR_AXE_Y
#define MILIEU_HORIZONTAL (DROITE_PLAN+GAUCHE_PLAN)/2
#define MILIEU_VERTICAL (BAS_PLAN+HAUT_PLAN)/2
#define UNITE_X (double) (MILIEU_HORIZONTAL - (GAUCHE_PLAN + 10))
/* +10 car la graduation est décalé par rapport au début/fin des axes */
#define UNITE_Y (double) (MILIEU_VERTICAL - (HAUT_PLAN  + 10))



/* *** affiche le plan *** */
void dessiner_plan_2_dimensions();

/* *** affiche la légende des coordonnées *** */
void affiche_legend_coord();

/* *** affiche les coordonnées x,y en bas à droite du plan *** */
void affiche_coord(int x, int y);

/* *** affiche un plan vide, permet d'effacer tous les points d'un plan *** */ 
void afficher_plan_vide();

#endif
