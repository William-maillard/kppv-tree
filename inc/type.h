/*****************************************************
 * Déclaration des types nécessaires pour travailler *
 * avec des points du plan en 2 dimensions:          *
 *    -point                                         *
 *    -tableau_de_points                             *
 *    -liste_KPPV                                    *
 *    -arbre_KPPV                                    *
 *****************************************************/
#ifndef _TYPE_H
#define _TYPE_H

#define NB_CLASSES 5

/* *** Structure de point *** */
typedef struct {
	float x;
	float y;
	unsigned int classe;
}point;

/* *** tableau qui va stocker les points *** */
typedef struct {
  point *tab;
  int nb_points;
  int taille;
}tableau_de_points;


/* *** Liste des K-PPV *** */
typedef struct cellule {
    point *point;
    float distance;
    struct cellule *suivant;
} voisin;

typedef voisin *liste_KPPV;


/* *** Arbre kd *** */
typedef enum{AXE_X, AXE_Y} axe_comparaison;

typedef struct struct_noeud{
  point point;
  axe_comparaison axe;
  struct struct_noeud *fils_gauche;
  struct struct_noeud *fils_droit;
}noeud;

typedef noeud *arbre_kd;

typedef struct {
  float xmin, xmax;
  float ymin, ymax;
}zone;

#endif
