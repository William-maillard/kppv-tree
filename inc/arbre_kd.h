/*********************************************************
 * Contient la déclaration du type arbre_kd et de zone.  *
 * les déclarations des fonctions pour créer un arbre kd *
 * et pour rechercher les k plus proche voisin dans      *
 * cet arbre.                                            *
 *********************************************************/
#ifndef _ARBRE_KD_H_
#define _ARBRE_KD_H_
#include "listes.h"
#include "type.h"

/* ***** fonctions générales sur un arbre ***** */

/* *** Renvoie un arbre vide *** */
arbre_kd arbre_kd_vide();

/* *** Retourne 1 si a est vide et 0 sinon *** */
int est_vide_arbre_kd(arbre_kd a);

/* *** créer un arbre contenant p à la racine et le renvoie *** */
arbre_kd creer_racine_arbre_kd(point p);

/* ***Fonction de libération de la mémoire*** */
void libere_mem_arbre(arbre_kd a);


/* ***** fonction pour créer un arbre kd ***** */

/* ***Insère un noeud dans un arbre kd, si il est vide alors crée la racine *** */
arbre_kd inserer_point_arbre_kd(arbre_kd a, point *p);


/* ***** fonction pour la recherche des k-ppv dans l'arbre kd ***** */
liste_KPPV rechercher(arbre_kd a, point *p, int k);

#endif
