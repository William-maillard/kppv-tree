/****************************************************
 * Déclarations des fonctions permettant d'afficher *
 * le résultat d'un input à coté du bouton          *
 * correspondant.                                   *
 * Des fonctions pour effacer ce résultat.          *
 ****************************************************/

#ifndef _AFFICHER_RESULTAT_INPUT_H_
#define _AFFICHER_RESULTAT_INPUT_H_
#include "type.h"

/**** La classe du point saisie. ****/
void afficher_classe_saisie(int classse);
void effacer_classe_saisie();


/**** Les coordonnées du point à effacer.(du dernier point cliqué) ****/
void afficher_coord_point_effacer(point p);
void effacer_coord_point_effacer();


/**** Le nom du fichier chargé. ****/
void afficher_nom_fichier_saisie(char *nom);
void effacer_nom_fichier_saisie();


/**** La valeur de k. ****/
void afficher_valeur_k_saisie(int k);
void effacer_valeur_k_saisie();

#endif
