/*****************************************************
 * Fonction principale du programme :                *
 *   -initialise les variables,                      *
 *   -initialise l'affichage,                        *
 *   -traite les évènements de la souris et modifie  *
 *    l'affichage/les variables en conséquence       *
 *    -libère la mémoire occupée.                    *
 *****************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <MLV/MLV_all.h>
#include "../inc/affichage_des_menus.h" /* qui inclus "bouton.h" et "type.h" */
#include "../inc/afficher_resultat_input.h"
#include "../inc/allocation.h"
#include "../inc/arbre_kd.h"
#include "../inc/coordonnees.h"
#include "../inc/dessiner_plan.h"
#include "../inc/evenement.h"
#include "../inc/listes.h"

int main()
{
    /*--les boutons déclarés dans bouton.h--*/

    /*--variables pour traiter les points--*/
    point point_clique;
    tableau_de_points t;
    arbre_kd a = arbre_kd_vide();
    liste_KPPV l_KPPV = liste_vide();
    int k = 0, nb_points_ajoutes = 0;

    /*--variables pour les évènements--*/
    MLV_Event event;
    MLV_Button_state state;
    int x, y;

    /*--autres--*/
    int continuer = 1;

    MLV_create_window("K-PPV", "K-PPV", WIDTH, HEIGHT);

    /* ********** ON CREE LE PLAN ********** */
    dessiner_plan_2_dimensions();
    affiche_legend_coord();

    /*on fait un bouton "spécial" pour le plan afin d' utiliser la fonction
      "est_clique(x,y,bouton)" pour savoir si la souris est dans le plan */
    b_plan = creer_bouton("plan");
    /* la graduation est decale de 10 par rapport au début/fin axe pour un aspect estétique.
       ensuite on ajoute un -1 car la fct est_clique utilise des inégalités strictes, et nous on veut
       inclure les points de coord x=1, x=-1 y=1, y=-1*/
    b_plan->x = GAUCHE_PLAN + 9;
    b_plan->y = HAUT_PLAN + 9;
    b_plan->width = LONGUEUR_AXE_X - 18;
    b_plan->height = LONGUEUR_AXE_Y - 18;

    /* *********** INITIALISATION DES VARIABLES ********** */
    creer_les_boutons();
    afficher_les_boutons_clickables(); /*affiche le mode création*/
    t.tab = NULL;
    t.nb_points = 0;
    t.taille = 0;

    /* ********** ON TRAITE LES EVENEMENTS ********** */
    while (continuer == 1)
    {
        event = MLV_get_event(NULL, NULL, NULL, NULL,
                              NULL, &x, &y, NULL, &state);

        switch (event)
        {
        case MLV_MOUSE_BUTTON:
            if (state == MLV_PRESSED)
            {
                /* ***** SI MODE CREATION ***** */
                if (b_creation->etat == 2)
                {
                    event_mode_creation(x, y, &t, &nb_points_ajoutes, &a);
                }

                else
                {
                    /* ***** MODE KPPV***** */
                    event_mode_KPPV(x, y, &k, &point_clique, t, &l_KPPV, a);
                }

                /* ***** REINITIALISATION ***** */
                if (est_clique(x, y, b_reinitialisation))
                {
                    /*--on réinitialise toutes les variables--*/
                    detruire_liste(&l_KPPV);
                    free(t.tab);
                    t.tab = NULL;
                    t.taille = 0;
                    t.nb_points = 0;
                    nb_points_ajoutes = 0;
                    k = 0;

                    /*--on efface le plan et les données affichées--*/
                    afficher_plan_vide();
                    effacer_classe_saisie();
                    effacer_coord_point_effacer();
                    effacer_valeur_k_saisie();
                    effacer_nom_fichier_saisie();

                    /*--si on est en mode kppv on passe en mode
                     création, car il nous faut des points dans
                     le tableau pour être en k-ppv*/
                    if (b_KPPV->etat == ACTIF)
                    {
                        change_mode(b_creation, b_KPPV);
                    }
                }

                /* ***** QUITTER ? ***** */
                else if (est_clique(x, y, b_quitter))
                {
                    continuer = 0;
                }
            }
            break;

        case MLV_MOUSE_MOTION:
            /*--on regarde si la souris survole le plan--*/
            if (est_clique(x, y, b_plan))
            {
                affiche_coord(x, y);
            }
            break;

        default:
            break;
        }
    }

    MLV_actualise_window();

    /* ***** LIBERATION MEMOIRE ****** */

    detruire_liste(&l_KPPV);
    free(t.tab);
    MLV_free_window();
    detruire_les_boutons();
    libere_mem_arbre(a);

    exit(0);
}
