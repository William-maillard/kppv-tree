/********************************************************************
 * Définitions des fonctions pour :                                 *
 *   -afficher un plan                                              *
 *   -aficher les coordonnées de la souris au survol du plan        *
 *   -effacer et afficher un nouveau plan                           *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <MLV/MLV_all.h>
#include "../inc/coordonnees.h"
#include "../inc/dessiner_plan.h"

void dessiner_plan_2_dimensions()
{
	int i, taille_graduation, position_graduation;

	/* ***** On dessine l'axe y ***** */
	/*--axe--*/
	MLV_draw_line(MILIEU_HORIZONTAL,
				  HAUT_PLAN,
				  MILIEU_HORIZONTAL,
				  BAS_PLAN,
				  MLV_COLOR_WHITE);

	/*--flèche--*/
	MLV_draw_line(MILIEU_HORIZONTAL - 5,
				  HAUT_PLAN,
				  MILIEU_HORIZONTAL,
				  HAUT_PLAN - 5,
				  MLV_COLOR_WHITE);

	MLV_draw_line(MILIEU_HORIZONTAL,
				  HAUT_PLAN - 5,
				  MILIEU_HORIZONTAL + 5,
				  HAUT_PLAN,
				  MLV_COLOR_WHITE);

	/* ***** On dessine l'axe x ***** */
	/*--axe--*/
	MLV_draw_line(GAUCHE_PLAN,
				  MILIEU_VERTICAL,
				  DROITE_PLAN,
				  MILIEU_VERTICAL,
				  MLV_COLOR_WHITE);

	/* flèche */
	MLV_draw_line(DROITE_PLAN,
				  MILIEU_VERTICAL - 5,
				  DROITE_PLAN + 5,
				  MILIEU_VERTICAL,
				  MLV_COLOR_WHITE);

	MLV_draw_line(DROITE_PLAN + 5,
				  MILIEU_VERTICAL,
				  DROITE_PLAN,
				  MILIEU_VERTICAL + 5,
				  MLV_COLOR_WHITE);

	/* ***** On dessine le cadre autour du plan ***** */

	MLV_draw_rectangle(GAUCHE_PLAN - PADDING,					/*x Nord Ouest*/
					   HAUT_PLAN - PADDING,						/*y N Ouest*/
					   DROITE_PLAN - GAUCHE_PLAN + 2 * PADDING, /*width*/
					   HAUT_PLAN - BAS_PLAN + 2 * PADDING,		/*heigth*/
					   MLV_COLOR_WHITE);

	/* *********** La graduation horizontale ********** */

	/*--initialisation des variables--*/
	taille_graduation = (DROITE_PLAN - GAUCHE_PLAN - 20) / NB_GRADUATIONS;
	/*un decalage de -10 est pris en compte (des deux cotés)
	  pour ne pas commencer la graduation au début de la ligne mais un peu après*/
	position_graduation = GAUCHE_PLAN + 10;

	/*--on place l'échelle--*/
	MLV_draw_text(position_graduation - 10,
				  MILIEU_VERTICAL + 5,
				  VALEUR_MIN_X,
				  MLV_COLOR_WHITE);

	MLV_draw_text(DROITE_PLAN - 15,
				  MILIEU_VERTICAL + 5,
				  VALEUR_MAX_X,
				  MLV_COLOR_WHITE);

	for (i = 0; i <= NB_GRADUATIONS; i++)
	{
		if (position_graduation != MILIEU_VERTICAL)
		{
			MLV_draw_line(position_graduation,
						  MILIEU_VERTICAL,
						  position_graduation,
						  MILIEU_VERTICAL + 2,
						  MLV_COLOR_WHITE);
		}
		position_graduation += taille_graduation;
	}

	/* ********** La graduation verticale ********** */

	/*--initialisation des variables-- */
	taille_graduation = (BAS_PLAN - HAUT_PLAN - 20) / NB_GRADUATIONS;
	position_graduation = HAUT_PLAN + 10;

	/*--on place l'échelle--*/
	MLV_draw_text(MILIEU_HORIZONTAL - 10,
				  position_graduation - 10,
				  VALEUR_MAX_Y,
				  MLV_COLOR_WHITE);

	MLV_draw_text(MILIEU_HORIZONTAL - 15,
				  BAS_PLAN - 15,
				  VALEUR_MIN_Y,
				  MLV_COLOR_WHITE);

	for (i = 0; i <= NB_GRADUATIONS; i++)
	{
		if (position_graduation != MILIEU_HORIZONTAL)
		{
			MLV_draw_line(MILIEU_HORIZONTAL,
						  position_graduation,
						  MILIEU_HORIZONTAL + 2,
						  position_graduation,
						  MLV_COLOR_WHITE);
		}
		position_graduation += taille_graduation;
	}

	MLV_actualise_window();
}

/* *** affiche la légende des coordonnées *** */
void affiche_legend_coord()
{
	/* on affichera en dessous les coordonnées (x,y)
	   au survol du plan par la souris */
	MLV_draw_text(DROITE_PLAN + PADDING + 2,
				  HEIGHT - 60,
				  "coordonnées :",
				  MLV_COLOR_WHITE);
	MLV_draw_text(DROITE_PLAN + PADDING + 2,
				  HEIGHT - 40,
				  "x = ",
				  MLV_COLOR_WHITE);
	MLV_draw_text(DROITE_PLAN + PADDING + 2,
				  HEIGHT - 20,
				  "y = ",
				  MLV_COLOR_WHITE);
}

/* *** affiche les coordonnées x,y en bas à droite du plan *** */
void affiche_coord(int x, int y)
{
	/* ****** CONVERSIONS ******/
	double x2 = coordx_to_point(x);
	double y2 = coordy_to_point(y);

	/* ***** AFFICHAGE ***** */
	/* width=71; height=21; interligne=3 */
	MLV_draw_text_box(DROITE_PLAN + PADDING + 30,
					  HEIGHT - 40,
					  71,
					  21,
					  "%lf",
					  3,
					  MLV_COLOR_BLACK,
					  MLV_COLOR_GREY,
					  MLV_COLOR_BLACK,
					  MLV_TEXT_CENTER,
					  MLV_HORIZONTAL_LEFT,
					  MLV_VERTICAL_CENTER,
					  x2);

	MLV_draw_text_box(DROITE_PLAN + PADDING + 30,
					  HEIGHT - 20,
					  71,
					  21,
					  "%lf",
					  3,
					  MLV_COLOR_BLACK,
					  MLV_COLOR_GREY,
					  MLV_COLOR_BLACK,
					  MLV_TEXT_CENTER,
					  MLV_HORIZONTAL_LEFT,
					  MLV_VERTICAL_CENTER,
					  y2);

	MLV_actualise_window();
}

/****
 * Affiche un plan vide, permet d'effacer tous les points d'un plan
 ****/
void afficher_plan_vide()
{
	/* on tace un rectangle noir sur le plan pour l'effacer*/
	MLV_draw_filled_rectangle(GAUCHE_PLAN, HAUT_PLAN,
							  2 * UNITE_X + 22, 2 * UNITE_Y + 22,
							  MLV_COLOR_BLACK);
	MLV_actualise_window();

	/* on réaffiche le plan */
	dessiner_plan_2_dimensions();
}
