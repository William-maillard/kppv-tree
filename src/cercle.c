/**************************************************
 * Fonction pour tracer un cercle de voisinage.   *
 * Pour cela, on utilise l'équation d'un cercle : *
 *   (x-x0)^2 + (y-y0)^2 = rayon^2                *
 * On prend x comme inconnue secondaire.          *
 * On va incrémenter x de i à chaque tour de      *
 * boucle et déterminer y avec la formule.        *
 * Puis on dessinera le cerlce à l'aide de la     *
 * fonction MLV_draw_polygon().                   *
 **************************************************/
#include <stdio.h>
#include <MLV/MLV_all.h>
#include <math.h>
#include "../inc/coordonnees.h"
#include "../inc/dessiner_plan.h"
#include "../inc/type.h"
#define NB_POINTS_CERCLE 400

/*fct pour insérer x et y dans leur liste réspectives, en vérifiant qu'il ne sort pas du plan,
  à la position nb_points*/
static void inserer_coords_liste(int x, int *liste_x, int y, int *liste_y, int nb_points)
{
  /*--vérification et insertion de x--*/
  if (x < GAUCHE_PLAN)
  {
    liste_x[nb_points] = GAUCHE_PLAN;
  }
  else if (x > DROITE_PLAN)
  {
    liste_x[nb_points] = DROITE_PLAN;
  }
  else
  {
    liste_x[nb_points] = x;
  }
  /*--vérification et insertion de y--*/
  if (y > BAS_PLAN)
  {
    liste_y[nb_points] = BAS_PLAN;
  }
  else if (y < HAUT_PLAN)
  {
    liste_y[nb_points] = HAUT_PLAN;
  }
  else
  {
    liste_y[nb_points] = y;
  }
}

/****
 * p0 : centre du cercle.
 * p : point appartenant au cercle.
 ****/
void dessiner_cercle(point p0, point p, MLV_Color c)
{
  int liste_x[NB_POINTS_CERCLE], liste_y[NB_POINTS_CERCLE];
  int rayon, x, y, x0, y0, xlim, nb_points, i;

  /* ***** INITIALISATION DES VARIABLES ***** */
  x = coordx_to_MLV(p.x);
  x0 = coordx_to_MLV(p0.x);
  y = coordy_to_MLV(p.y);
  y0 = coordy_to_MLV(p0.y);
  rayon = sqrt(pow(x - x0, 2) + pow(y - y0, 2)) + 1;
  /*+1 :comme on arrondie, on s'assure que le points sea dans le cercle*/
  /*on trace le cercle en partant d'un point gauche aligné horizontalement avec le centre du cercle*/
  x = x0 - rayon;
  y = y0;
  xlim = x + 2 * rayon;
  nb_points = 0; /*pour conaitre le nb de points dans les listes*/
  i = (8 * rayon) / (NB_POINTS_CERCLE);
  /*pour chaque demi cercle on a NB_POINTS_CERCLE/2 pour aller de x à xmax, donc on a
    une distance de 4*le rayon à 'découper' selon NB_POINTS_CERCLE/2*/
  if (i == 0)
  {
    i = 1; /*pour les petits rayons, on s'assure que i != 0*/
  }

  /* ***** CALCULE DE LA LISTE DES COORDONNEES ***** */

  /*-- tracé du premier demi-cercle --*/
  while (x <= xlim)
  {
    inserer_coords_liste(x, liste_x, y, liste_y, nb_points);
    /*--on affiche le point juste pour le suivi du tracé--*/
    MLV_draw_pixel(liste_x[nb_points], liste_y[nb_points], MLV_COLOR_BLUE);
    MLV_actualise_window();
    /*--on actualise les variables--*/
    nb_points++;
    x += i;
    y = sqrt(pow(rayon, 2) - pow((x - x0), 2)) + y0;
  }
  /*--on se place coté droit du cercle--*/
  x = xlim;
  xlim = x - 2 * rayon;
  y = y0;
  /*--tracé de l'autre moitié--*/
  while (x >= xlim)
  {
    inserer_coords_liste(x, liste_x, y, liste_y, nb_points);
    /*on affiche le point juste pour le suivi du tracé */
    MLV_draw_pixel(liste_x[nb_points], liste_y[nb_points], MLV_COLOR_BLUE);
    MLV_actualise_window();
    /*--on actualise les variables--*/
    nb_points++;
    x -= i;
    y = -sqrt(pow(rayon, 2) - pow(x - x0, 2)) + y0;
  }
  MLV_wait_seconds(1);

  /* ***** AFFICHAGE DU POLYGONE ***** */
  MLV_draw_polygon(liste_x, liste_y, nb_points, c);
  MLV_actualise_window();
}
