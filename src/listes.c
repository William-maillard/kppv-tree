/*************************************
 * définitions des fonctions pour :  *
 *   -créer un liste vide            *
 *   -tester si une liste est vide   *
 *   -libérer la mémoire d'un liste  *
 *************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../inc/allocation.h"
#include "../inc/type.h"

/* *** renvoie une liste vide *** */
liste_KPPV liste_vide()
{
    return NULL;
}

/* *** teste si une liste_KPPV est vide *** */
int est_liste_vide(liste_KPPV l)
{
    if (l == liste_vide())
    {
        return 1;
    }
    return 0;
}

/* *** fonction qui libère l'espace mémoire occupé par la liste et affecte liste_vide à l *** */
void detruire_liste(liste_KPPV *l)
{
    liste_KPPV p;

    while (!est_liste_vide(*l))
    {
        p = (*l);
        (*l) = (*l)->suivant;
        free(p);
    }
    (*l) = liste_vide();
}

/* ***
 * Fonction de calcul de la distance euclidienne, utilisé pour trier
 * la liste
 *** */
float distance_euclidienne(point *pt1, point *pt2)
{
    return sqrt(pow(pt2->x - pt1->x, 2) + pow(pt2->y - pt1->y, 2));
}

/* ***
 * Fonction d'insertion d'un voisin pt (dont les champs on déjà étés remplis)
 * dans une liste triée par ordre de distance euclidienne donné en paramètre
 *** */
typedef enum
{
    CROISSANT,
    DECROISSANT
} tri;

liste_KPPV insere_liste_trie(liste_KPPV l, voisin *pt, tri tri)
{
    liste_KPPV parcours = l; /* va nous servir à parcourir la liste */

    /* si la liste est vide */
    if (est_liste_vide(l))
    {
        return pt;
    }
    /* s'il faut insérer au début */
    if ((tri == CROISSANT && l->distance > pt->distance) || (tri == DECROISSANT && l->distance < pt->distance))
    {
        pt->suivant = l;
        return pt;
    }
    /* sinon, on parcours la liste pour trouver où l'insérer */
    while (!est_liste_vide(parcours->suivant) &&
           ((tri == CROISSANT && parcours->suivant->distance < pt->distance) ||
            (tri == DECROISSANT && parcours->suivant->distance > pt->distance)))
    {
        parcours = parcours->suivant;
    }
    /* on l'insère */
    pt->suivant = parcours->suivant;
    parcours->suivant = pt;
    /* on renvoie le pointeur sur le début de la liste */
    return l;
}
