/***********************************************************
 * Fonctions restreintes au module                         *
 *   -dessiner un points (x, y) avec différentes formes :  *
 *     -croix droite       +                               *
 *     -croix oblique      x                               *
 *     -trait horizontal   -                               *
 *     -trait vertical     |                               *
 *     (x,y) coord du milieu de la forme                   *
 *                                                         *
 * Fonctions à utiliser hors du module:                    *
 *   -afficher un point selon sa classe et son état        *
 *   -afficher les points d'un tableau de points           *
 *   -traiter un point cliqué (création de la structure +  *
 *                             affichage)                  *
 ***********************************************************/
#include <stdlib.h>
#include <MLV/MLV_all.h>
#include "../inc/coordonnees.h"
#include "../inc/dessiner_point.h"
#include "../inc/type.h"

/* *** Fonctions pour la forme du point *** */

static void dessiner_croix(int x, int y, MLV_Color color)
{
  /* *** Ligne horizontale du '+' *** */
  MLV_draw_line(x - 2, y, x + 2, y, color);

  /* ***Ligne verticale du '+'*** */
  MLV_draw_line(x, y - 2, x, y + 2, color);
}

static void dessiner_croix_oblique(int x, int y, MLV_Color color)
{
  /* *** Ligne horizontale du '+' *** */
  MLV_draw_line(x - 2, y + 2, x + 2, y - 2, color);

  /* ***Ligne verticale du '+'*** */
  MLV_draw_line(x + 2, y + 2, x - 2, y - 2, color);
}

static void dessiner_trait_horizontal(int x, int y, MLV_Color color)
{
  MLV_draw_line(x - 2, y, x + 2, y, color);
}

static void dessiner_trait_vertical(int x, int y, MLV_Color color)
{
  MLV_draw_line(x, y - 2, x, y + 2, color);
}

static void dessiner_etoile(int x, int y, MLV_Color color)
{
  dessiner_croix_oblique(x, y, color);
  dessiner_croix(x, y, color);
}

/****
 * Fonction qui affiche ou efface le symbole d'un point suivant sa classe
 * x et y sont les coordonnées entières du point.
 ****/
void affiche_point_classe(point pt, etat_point etat)
{
  MLV_Color c;
  int classe, x, y;

  x = coordx_to_MLV(pt.x);
  y = coordy_to_MLV(pt.y);

  if (etat == EN_VALEUR)
  {
    classe = 0;
  }
  else
  {
    classe = pt.classe;
  }

  switch (classe)
  {
  case 1:
    if (etat == EFFACE)
    {
      c = MLV_COLOR_BLACK;
    }
    else
    {
      c = MLV_COLOR_BLUE;
    }
    dessiner_trait_horizontal(x, y, c);
    break;

  case 2:
    if (etat == EFFACE)
    {
      c = MLV_COLOR_BLACK;
    }
    else
    {
      c = MLV_COLOR_RED;
    }
    dessiner_croix(x, y, c);
    break;

  case 3:
    if (etat == EFFACE)
    {
      c = MLV_COLOR_BLACK;
    }
    else
    {
      c = MLV_COLOR_GREEN;
    }
    dessiner_croix_oblique(x, y, c);
    break;

  case 4:
    if (etat == EFFACE)
    {
      c = MLV_COLOR_BLACK;
    }
    else
    {
      c = MLV_COLOR_PURPLE;
    }
    dessiner_etoile(x, y, c);
    break;

  case 5:
    if (etat == EFFACE)
    {
      c = MLV_COLOR_BLACK;
    }
    else
    {
      c = MLV_COLOR_YELLOW;
    }
    dessiner_trait_vertical(x, y, c);
    break;

  default:
    if (etat == EFFACE)
    {
      c = MLV_COLOR_BLACK;
    }
    else
    {
      c = MLV_COLOR_WHITE;
    }
    MLV_draw_circle(x, y, 2, c);
    break;
  }
  MLV_actualise_window();
}

/****
 * Fonction qui affiche tous les points d'un tableau_de_points
 ****/
void afficher_tableau_de_points(tableau_de_points t)
{
  int i, n = t.nb_points;

  for (i = 0; i < n; i++)
    affiche_point_classe(t.tab[i], AFFICHE);
}

/* *** Fonction qui crée la structure point du point cliqué, et qui affiche ce point.
   La fonction retourne la structure point *** */
point affiche_point_clique(int x, int y)
{
  point p;

  p.x = coordx_to_point(x);
  p.y = coordy_to_point(y);
  p.classe = 0;
  /* Remarque : la conversion entier->floattant puis floattant->entier
     peut amener à un déclage par rapport au point cliqué, donc on fait cette conversion
     pour que lorsqu'on efface le point cliqué pour ensuite l'afficher selon sa classe,
     il n'y est pas de traces du point d'avant*/
  affiche_point_classe(p, AFFICHE);

  return p;
}
