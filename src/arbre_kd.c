/*****************************************************
 * Définition des fonctions pour :                   *
 *   -définition de l'état dun point                 *
 *   -afficher un point selon sa classe et son état  *
 *   -afficher les points d'un tableau_de_points     *
 *   -traiter un point cliqué                        *
 *    (création de la structure + affichage)         *
 *****************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <MLV/MLV_all.h>
#include "../inc/allocation.h"
#include "../inc/arbre_kd_graphique.h"
#include "../inc/bouton.h"
#include "../inc/coordonnees.h"
#include "../inc/dessiner_plan.h"
#include "../inc/listes.h"

/*pour le debogage */
void taille_liste(liste_KPPV l)
{
  int t = 0;

  printf("TAILLE LISTE : ");
  while (!est_liste_vide(l))
  {
    t++;
    l = l->suivant;
  }
  printf("%d\n", t);
}
/* ***** Fonction générales sur les arbres ***** */
/****
 * Renvoie un pointeur sur un arbre_kd NULL
 ****/
arbre_kd arbre_kd_vide()
{
  return NULL;
}

/****
 * Retourne 1 si a est vide et 0 sinon
 ****/
int est_vide_arbre_kd(arbre_kd a)
{
  if (a == arbre_kd_vide())
  {
    return 1;
  }
  return 0;
}

/****
 * Crée la racine d'un arbre kd
 ****/
arbre_kd creer_racine_arbre_kd(point p)
{
  arbre_kd a = (arbre_kd)allocation_mem(1, sizeof(noeud));

  a->point = p;
  a->axe = AXE_X;
  a->fils_gauche = arbre_kd_vide();
  a->fils_droit = arbre_kd_vide();

  return a;
}

/* ***Fonction de libération de la mémoire*** */
void libere_mem_arbre(arbre_kd a)
{
  if (a != arbre_kd_vide())
  {
    libere_mem_arbre(a->fils_gauche);
    libere_mem_arbre(a->fils_droit);
    free(a);
  }
}

/* ***** Fonctions pour créer un arbre kd ***** */

/****
 * Retourne 1 si le point de l'arbre a1 est inférieur à celui de a2
 * suivant l'axe de comparaison de a2 et 0 sinon
 ****/
static int est_avant(arbre_kd a1, arbre_kd a2)
{

  if ((a2->axe == AXE_X && a1->point.x <= a2->point.x) || (a2->axe == AXE_Y && a1->point.y <= a2->point.y))
    return 1;
  return 0;
}

/****
 * Fonction récursive d'insertion d'un noeud dans un arbre_kd
 ****/
static void inserer_noeud_arbre_kd(arbre_kd new_node, arbre_kd a)
{
  if (est_avant(new_node, a))
  {
    if (a->fils_gauche == arbre_kd_vide())
    {
      /*--cas d'arrêt de la récursivité, on insère le noeud--*/
      new_node->axe = (a->axe + 1) % 2;
      a->fils_gauche = new_node;
    }
    else
      /*--on l'insère récursivement dans la brache gauche--*/
      inserer_noeud_arbre_kd(new_node, a->fils_gauche);
  }
  else
  {
    if (a->fils_droit == arbre_kd_vide())
    {
      /*--cas d'arrêt de la récursivité, on insère le noeud--*/
      new_node->axe = (a->axe + 1) % 2;
      a->fils_droit = new_node;
    }
    else
      /*--on l'insère récursivement dans la brache droite--*/
      inserer_noeud_arbre_kd(new_node, a->fils_droit);
  }
}

/* ***
 * Insère un noeud dans un arbre kd, si il est vide alors crée la racine.
 *** */
arbre_kd inserer_point_arbre_kd(arbre_kd a, point *p)
{
  arbre_kd new_node = (arbre_kd)allocation_mem(1, sizeof(noeud));

  new_node->point = *p;
  new_node->fils_gauche = arbre_kd_vide();
  new_node->fils_droit = arbre_kd_vide();

  if (est_vide_arbre_kd(a))
  {
    new_node->axe = AXE_X;
    return new_node;
  }
  inserer_noeud_arbre_kd(new_node, a);

  return a;
}

/* ********* Recherche KPPV ********* */

/***
 * Retourne 1 si un point est dans la zone et 0 sinon
 ***/
static int est_dans_zone(point *p, zone z_tmp)
{
  if ((p->x >= z_tmp.xmin && p->x <= z_tmp.xmax) && (p->y >= z_tmp.ymin && p->y <= z_tmp.ymax))
  {
    return 1;
  }
  return 0;
}

/***
 * Met à jour la liste des K-PPV en ajoutant ou ignorant un point
 * Cette fonction va insérer le point dans une liste triée par ordre décroissant
 * Retourne la liste
 ***/
static liste_KPPV maj_liste(point *p_tmp, point *p_clique, liste_KPPV l)
{
  liste_KPPV new_cell = NULL, l2 = NULL;
  float distance = distance_euclidienne(p_clique, p_tmp);

  /*--on test si on doit insérer le point dans la liste--*/
  if (distance < l->distance)
  {
    /*--on libère le voisin le plus loin i.e le premier de la liste--*/
    l2 = l;
    l = l->suivant;
    free(l2);

    /*--Création du voisin*/
    new_cell = (liste_KPPV)allocation_mem(1, sizeof(voisin));
    new_cell->point = p_tmp;
    new_cell->distance = distance;
    new_cell->suivant = NULL;

    /*--insertion--*/
    l = insere_liste_trie(l, new_cell, DECROISSANT);
  }
  return l;
}

/***
 * Cherche le point le plus proche de p appartenant à z_tmp et
 * renvoie la distance auclidienne entre ces deux points.
 ***/
static float point_proche_dans_zone(point *p, zone z_tmp)
{
  point p_zone;

  /* ***** Sélection de la coordonnée en x ***** */
  if (p->x >= z_tmp.xmax)
  {
    /*--p est sur la droite--*/
    p_zone.x = z_tmp.xmax;
  }
  else if (p->x <= z_tmp.xmin)
  {
    /*--p est sur la gauche--*/
    p_zone.x = z_tmp.xmin;
  }
  else
  {
    /*--p est aligné  verticalement avec la zone--*/
    p_zone.x = p->x;
  }

  /* ***** Sélection de la coordonnée en y ***** */
  if (p->y >= z_tmp.ymax)
  {
    /*--p est au-dessus de la zone--*/
    p_zone.y = z_tmp.ymax;
  }
  else if (p->y <= z_tmp.ymin)
  {
    /*--p est en dessous de la zone--*/
    p_zone.y = z_tmp.ymin;
  }
  else
  {
    /*--p est aligné horizontalement avec la zone--*/
    p_zone.y = p->y;
  }

  return distance_euclidienne(p, &p_zone);
}

/* ***
 * Renvoie la liste des k-ppv de p_clique
 *** */
liste_KPPV rechercher_aux(arbre_kd a, point *p_clique, zone zone_courante, liste_KPPV l, int k, int *taille)
{
  zone zone_droite = zone_courante, zone_gauche = zone_courante;
  float distance_gauche = 3., distance_droite = 3.; /*pas à 2 car 'floatting point erreur' peut donner des d>2.*/
  liste_KPPV new_cell = NULL;

  if (est_vide_arbre_kd(a))
  {
    return l;
  }

  if (b_animation->etat == ACTIF)
  {
    annimation(zone_courante, MLV_COLOR_GREEN, 1000);
  }
  /*--traitement du point du noeud courant--*/
  if (*taille < k)
  {
    /*--création d'un nouveau noeud--*/
    new_cell = (liste_KPPV)allocation_mem(1, sizeof(voisin));
    new_cell->point = &(a->point);
    new_cell->distance = distance_euclidienne(new_cell->point, p_clique);
    new_cell->suivant = liste_vide();

    /*--on l'insère dans la liste*/
    l = insere_liste_trie(l, new_cell, DECROISSANT);
    (*taille)++;
  }
  else
  {
    l = maj_liste(&(a->point), p_clique, l);
  }

  /*--mise à jour des coordonnées des zones--*/
  if (a->axe == AXE_X)
  {
    zone_gauche.xmax = a->point.x;
    zone_droite.xmin = a->point.x;
  }
  else
  {
    zone_gauche.ymax = a->point.y;
    zone_droite.ymin = a->point.y;
  }

  /*--recherche des zones à explorer--*/

  if (!est_vide_arbre_kd(a->fils_gauche))
  {
    if (est_dans_zone(p_clique, zone_gauche))
    {
      /*--alors on explore cette branche--*/
      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_WHITE, 500);
      }

      l = rechercher_aux(a->fils_gauche, p_clique, zone_gauche, l, k, taille);

      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_BLUE, 1000);
      }

      if (!est_vide_arbre_kd(a->fils_droit))
      {
        /*--on test si le fils_droit peut contenir un k-ppv--*/
        distance_droite = point_proche_dans_zone(p_clique, zone_droite);
        if (*taille < k || distance_droite < l->distance)
        {
          if (b_animation->etat == ACTIF)
          {
            annimation(zone_courante, MLV_COLOR_WHITE, 500);
          }

          /*--la branche contient un possible k-ppv, on l'explore--*/
          return rechercher_aux(a->fils_droit, p_clique, zone_droite, l, k, taille);
        }
      }
      return l;
    }
    else
    {
      distance_gauche = point_proche_dans_zone(p_clique, zone_gauche);
    }
  }
  /*--on regarde si p est dans la zone droite--*/
  if (!est_vide_arbre_kd(a->fils_droit))
  {
    if (est_dans_zone(p_clique, zone_droite))
    {
      /*--alors on explore cette branche*/
      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_WHITE, 500);
      }

      l = rechercher_aux(a->fils_droit, p_clique, zone_droite, l, k, taille);

      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_BLUE, 1000);
      }

      /*--on test si le fils gauche peut contenir un k-ppv--*/
      if (*taille < k || distance_gauche < l->distance)
      {
        if (b_animation->etat == ACTIF)
        {
          annimation(zone_courante, MLV_COLOR_WHITE, 500);
        }

        /*--la branche contient un possible k-ppv, on l'explore--*/
        return rechercher_aux(a->fils_gauche, p_clique, zone_gauche, l, k, taille);
      }
      return l;
    }
    else
    {
      distance_droite = point_proche_dans_zone(p_clique, zone_droite);
    }
  }
  /*--sinon aucunne des zones gauche et droite ne contient le point cliqué.
    Si le fils n'est pas null alors le distance de son point le plus
    proche avec p_clique a été calculé--*/
  if (distance_gauche != 3. || distance_droite != 3.)
  {
    /*--on sélectionne la plus petite distance et si taille liste < k on parcours aussi l'autre branche--*/
    if (distance_gauche < distance_droite)
    {
      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_WHITE, 500);
      }

      l = rechercher_aux(a->fils_gauche, p_clique, zone_gauche, l, k, taille);

      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_BLUE, 500);
      }

      if ((*taille < k && distance_droite != 3.) || distance_droite < l->distance)
      {
        l = rechercher_aux(a->fils_droit, p_clique, zone_droite, l, k, taille);
      }
    }
    else
    {
      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_WHITE, 500);
      }

      l = rechercher_aux(a->fils_droit, p_clique, zone_droite, l, k, taille);

      if (b_animation->etat == ACTIF)
      {
        annimation(zone_courante, MLV_COLOR_BLUE, 500);
      }

      if ((*taille < k && distance_gauche != 3.) || distance_gauche < l->distance)
      {
        l = rechercher_aux(a->fils_gauche, p_clique, zone_gauche, l, k, taille);
      }
    }
  }
  /*--sinon les fils sont tous deux vides, on ne fait rien--*/
  if (b_animation->etat == ACTIF)
  {
    annimation(zone_courante, MLV_COLOR_WHITE, 500);
  }
  return l;
}

liste_KPPV rechercher(arbre_kd a, point *p, int k)
{
  zone zone_depart = {-1, 1, -1, 1};
  liste_KPPV l = liste_vide();
  int taille = 0;

  l = rechercher_aux(a, p, zone_depart, l, k, &taille);

  return l;
}
