/*****************************************************
 * Fichier qui contient les fonctions permettant de  *
 * -creer un bouton                                  *
 * -afficher un bouton avec MLV                      *
 * -dire si un bouton est cliqué                     *
 * -supprimer un bouton                              *
 *****************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MLV/MLV_all.h>
#include "../inc/bouton.h"
#include "../inc/allocation.h"

/*_______________________________________*/

/* ***** LES BOUTONS DU PROGRAMME ***** */
/*--boutons divers--*/
bouton b_creation, b_KPPV, b_reinitialisation, b_quitter, b_effacer;
/*--boutons pour les inputs--*/
bouton b_sauvegarde, b_valeur_k, b_classe, b_chargement;
/*--boutons pour les options d'affichage--*/
bouton b_voisinage, b_decision, b_decoupe_plan, b_animation;
/*--boutons pour le type de structure utilisée--*/
bouton b_tableau, b_arbre;
/*permet d'utiliser la fct est_clique() pour savoir quand on clique dans le plan*/
bouton b_plan;
/*_______________________________________*/

/****
 * Fonction qui créer un bouton "name", sans initialiser ces dimensions
 *** */
bouton creer_bouton(char *name)
{
  bouton b;

  /* ***** Reservation memoire ***** */
  /*--d'une strcut bouton--*/
  b = (bouton)allocation_mem(1, sizeof(struct struct_bouton));
  /*--du nom--*/
  b->name = (char *)allocation_mem(strlen(name) + 1, sizeof(char));
  strcpy(b->name, name);
  /*--inactif--*/
  b->etat = VISIBLE;

  return b;
}

/****
 * Créer les boutons du programme.
 ****/
void creer_les_boutons()
{
  b_creation = creer_bouton("mode création");
  b_KPPV = creer_bouton("mode K-PPV");
  b_reinitialisation = creer_bouton("reinitialisation de\nla fenêtre");
  b_quitter = creer_bouton("Quitter");
  b_effacer = creer_bouton("effacer le\ndernier point");

  b_tableau = creer_bouton("tableau");
  b_arbre = creer_bouton("arbre");

  b_voisinage = creer_bouton("voisinage");
  b_decision = creer_bouton("avec prise de décision");
  b_decoupe_plan = creer_bouton("decoupe du plan");
  b_animation = creer_bouton("animation recherche k-ppv");

  b_sauvegarde = creer_bouton("Sauvegarde des données\nentrées");
  b_valeur_k = creer_bouton("k= ");
  b_classe = creer_bouton("classe : ");
  b_chargement = creer_bouton("chemin fihier : ");
}

/******
 * Fonction qui affiche un bouton dont les champs ont tous étés initialisés
 * la couleur dépend du champ etat.
 ******/
void afficher_bouton(bouton b)
{
  MLV_Color c;

  switch (b->etat)
  {
  case INVISIBLE:
    /* bouton invisible = désactivé */
    c = MLV_COLOR_BLACK;
    break;
  case VISIBLE:
    /* bouton visible = inactif */
    c = MLV_COLOR_WHITE;
    break;
  case ACTIF:
    /* bouton actif */
    c = MLV_COLOR_GREEN;
    break;
  default:
    /* pour le bouton quitter */
    c = MLV_COLOR_RED;
  }

  MLV_draw_adapted_text_box(b->x,
                            b->y,
                            b->name,
                            4,
                            c,
                            c,
                            MLV_COLOR_BLACK,
                            MLV_TEXT_CENTER);

  MLV_actualise_window();
}

/******
 * Fonction qui renvoie 1 si les coordonnées (x,y) sont situe dans le bouton,
 * 0 sinon
 ******/
int est_clique(int x, int y, bouton b)
{
  int xmax = b->x + b->width;
  int ymax = b->y + b->height;

  if ((x > b->x && x < xmax) && (y > b->y && y < ymax))
  {
    return 1;
  }
  return 0;
}

/*****
 * Fonction qui libère la mémoire occupé par un bouton
 *****/
void supprimer_bouton(bouton *b)
{
  free((*b)->name);
  free(*b);
  *b = NULL;
}

/* ***
 * libère la mémoire occupé par tous les boutons du programme
 *** */
void detruire_les_boutons()
{
  supprimer_bouton(&b_creation);
  supprimer_bouton(&b_KPPV);
  supprimer_bouton(&b_reinitialisation);
  supprimer_bouton(&b_quitter);
  supprimer_bouton(&b_effacer);

  supprimer_bouton(&b_tableau);
  supprimer_bouton(&b_arbre);

  supprimer_bouton(&b_voisinage);
  supprimer_bouton(&b_decision);
  supprimer_bouton(&b_decoupe_plan);
  supprimer_bouton(&b_animation);

  supprimer_bouton(&b_sauvegarde);
  supprimer_bouton(&b_valeur_k);
  supprimer_bouton(&b_classe);
  supprimer_bouton(&b_chargement);
}
