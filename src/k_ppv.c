/**************************************************************
 * Définitions des fonctions implémantant le mode k-ppv :     *
 *   -créer la liste des voisins                              *
 *   -attribuer une classe au point en fonction de ces k-ppv  *
 *   -afficher le cercle de voisinage                         *
 *   -la mise en avant des k-ppv                              *
 **************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <MLV/MLV_all.h>
#include "../inc/allocation.h"
#include "../inc/bouton.h"
#include "../inc/coordonnees.h"
#include "../inc/dessiner_plan.h"
#include "../inc/dessiner_point.h"
#include "../inc/k_ppv.h" /* inclus liste.h et type.h */

/* ***
 * Crée et renvoie la liste des voisins
 *** */
liste_KPPV creer_liste_voisins(tableau_de_points t, point pt)
{
  liste_KPPV l = liste_vide();
  liste_KPPV voisin_courrant;
  int i, n = t.nb_points;

  /* on parcours le tableau de points, et pour chaque point on calcule sa distance
     euclidienne par rapport à pt puis on l'insère dans la liste l*/
  for (i = 0; i < n; i++)
  {
    /* ***** creation d'un nouveau voisin ***** */
    voisin_courrant = (liste_KPPV)allocation_mem(1, sizeof(voisin));
    voisin_courrant->point = &(t.tab[i]);
    voisin_courrant->distance = distance_euclidienne(&pt, voisin_courrant->point);
    voisin_courrant->suivant = liste_vide();

    /* ***** insertion dans la liste des KPPV ***** */
    l = insere_liste_trie(l, voisin_courrant, CROISSANT);
  }

  return l; /* on renvoie la liste des voisins triée */
}

/* ***
 * attribut une classe à un point en fonction de ses k voisins
 *** */
void kppv_decision(liste_KPPV l, point *pt, int k)
{
  int compteur[NB_CLASSES + 1], i; /*à l'indice i le nombre de points de classe i*/
  unsigned int classe;

  /* initialisation des compteurs */
  for (i = 1; i <= NB_CLASSES; i++)
  {
    compteur[i] = 0;
  }

  /* calcul du nombre de chaque classes */
  for (i = 0; i < k; i++)
  {
    compteur[l->point->classe]++;
    l = l->suivant;
  }

  /* on détermine la classe du point */
  classe = 1;
  for (i = 2; i <= NB_CLASSES; i++)
  {
    if (compteur[i] > compteur[classe])
    {
      classe = i;
    }
  }
  pt->classe = classe;
}

/* ***
 * met en avant les k premiers éléments d'une liste_KPPV
 * retourne le pointeur sur le k° voisin (utilisé pour l'actualisation du plan)
 *** */
liste_KPPV mise_en_avant_k_ppv(int k, liste_KPPV l)
{
  int i;

  for (i = 1; i < k; i++)
  {
    affiche_point_classe(*(l->point), EN_VALEUR);
    l = l->suivant;
  }
  affiche_point_classe(*(l->point), EN_VALEUR);

  return l;
}

/* ***
 * supprime la mise en avant des k éléments de la liste
 *** */
void supprime_mise_en_avant(int k, liste_KPPV l)
{
  unsigned int classe;
  int i;

  for (i = 0; i < k; i++)
  {
    /*--on efface la mise en valeur--*/
    classe = l->point->classe;
    l->point->classe = 0;
    affiche_point_classe(*(l->point), EFFACE);

    /*--on reafiche le point celon sa classe--*/
    l->point->classe = classe;
    affiche_point_classe(*(l->point), AFFICHE);

    /*--point suivant--*/
    l = l->suivant;
  }
}
