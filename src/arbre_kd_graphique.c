/***********************************************************
 * Contient les options d'affichage proposés en mode k-ppv *
 * pour l'arbre kd.                                        *
 ***********************************************************/
#include <stdio.h>
#include <MLV/MLV_all.h>
#include "../inc/arbre_kd.h"
#include "../inc/coordonnees.h"
#include "../inc/dessiner_plan.h"
#include "../inc/dessiner_point.h"

/* ***
 * Dessine une droite pointillé de (x, y1) à (x, y2)
 *** */
static void dessiner_droite_pointille_verticale(int x, int ymin, int ymax, MLV_Color c)
{
  /*ymin est situé en bas de l'écran donc en coordonnées entière ymin > ymax*/
  while (ymax < ymin)
  {
    MLV_draw_line(x, ymax,
                  x, ymax + 10,
                  c);
    MLV_actualise_window();
    ymax += 20;
  }
}
static void dessiner_droite_pointille_horizontale(int y, int x1, int x2, MLV_Color c)
{
  while (x1 < x2)
  {
    MLV_draw_line(x1, y,
                  x1 + 10, y,
                  c);
    MLV_actualise_window();
    x1 += 20;
  }
}
/* ***
 * Dessine la découpe du plan faite par l'arbre a
 *** */
static void decoupe_plan_aux(arbre_kd a, zone z, MLV_Color c)
{
  float tmps;

  /*--si ce n'est pas une feuille, on coupe le plan--*/
  if (!est_vide_arbre_kd(a->fils_gauche) || !est_vide_arbre_kd(a->fils_droit))
  {
    /*--affichage de la coupe--*/
    if (a->axe == AXE_X)
    {
      dessiner_droite_pointille_verticale(coordx_to_MLV(a->point.x), coordy_to_MLV(z.ymin), coordy_to_MLV(z.ymax), c);
      if (a->fils_gauche != NULL)
      {
        tmps = z.xmax;
        z.xmax = a->point.x;
        decoupe_plan_aux(a->fils_gauche, z, c);
        z.xmax = tmps;
      }
      if (a->fils_droit != NULL)
      {
        z.xmin = a->point.x;
        decoupe_plan_aux(a->fils_droit, z, c);
      }
    }
    else
    {
      dessiner_droite_pointille_horizontale(coordy_to_MLV(a->point.y), coordx_to_MLV(z.xmin), coordx_to_MLV(z.xmax), c);
      if (a->fils_gauche != NULL)
      {
        tmps = z.ymax;
        z.ymax = a->point.y;
        decoupe_plan_aux(a->fils_gauche, z, c);
        z.ymax = tmps;
      }
      if (a->fils_droit != NULL)
      {
        z.ymin = a->point.y;
        decoupe_plan_aux(a->fils_droit, z, c);
      }
    }
  }
}
void decoupe_plan(arbre_kd a, MLV_Color c)
{
  zone z = {-1., 1., -1., 1.};

  decoupe_plan_aux(a, z, c);
}

/* ***
 * animation de la recherche, dessine la zone courante.
 *** */
void annimation(zone z, MLV_Color c, int ms)
{
  int xmin, xmax, ymin, ymax;
  xmin = coordx_to_MLV(z.xmin);
  xmax = coordx_to_MLV(z.xmax);
  ymin = coordy_to_MLV(z.ymin);
  ymax = coordy_to_MLV(z.ymax);

  /*--dessinne : haut, droite, bas, gauche--*/
  dessiner_droite_pointille_horizontale(ymax, xmin, xmax, c);
  dessiner_droite_pointille_verticale(xmax, ymin, ymax, c);
  dessiner_droite_pointille_horizontale(ymin, xmin, xmax, c);
  dessiner_droite_pointille_verticale(xmin, ymin, ymax, c);

  MLV_actualise_window();
  MLV_wait_milliseconds(ms);
}
