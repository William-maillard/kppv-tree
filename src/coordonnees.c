/*********************************************************
 * Déclartions des fonctions permettant de convertir les *
 * coordonnées double du point en coordonnées entières   *
 * de MLV et inversement.                                *
 *********************************************************/
#include <stdlib.h>
#include "../inc/dessiner_plan.h"
#include "../inc/type.h"

/* ***
 * Convertie la coordonnée x ou y et la renvoie
 *** */
int coordx_to_MLV(double x)
{
  return (int)(x * UNITE_X) + MILIEU_HORIZONTAL;
}
int coordy_to_MLV(double y)
{
  return (int)(-y * UNITE_Y) + MILIEU_VERTICAL;
}

/* ***
 * Converti la coordonnée x ou y en float
 *** */
double coordx_to_point(int x)
{
  return (x - MILIEU_HORIZONTAL) / UNITE_X;
}
double coordy_to_point(int y)
{
  return (MILIEU_VERTICAL - y) / UNITE_Y;
}
