/************************************************
 * Contient les fonctions permettant d'afficher *
 * le résultat d'un input à coté du bouton      *
 * correspondant.                               *
 * Des fonctions pour effacer ce résultat.      *
 ************************************************/

#include <stdlib.h>
#include <MLV/MLV_all.h>
#include "../inc/bouton.h"
#include "../inc/type.h"
#include "../inc/dessiner_plan.h"

/****
 * La classe du point saisie.
 ****/
void afficher_classe_saisie(int classe)
{
    MLV_draw_text_box(b_classe->x + b_classe->width, b_classe->y,
                      b_classe->width, b_classe->height,
                      "%3d",
                      3,
                      MLV_COLOR_BLACK,
                      MLV_COLOR_GREY,
                      MLV_COLOR_BLACK,
                      MLV_TEXT_CENTER,
                      MLV_HORIZONTAL_LEFT,
                      MLV_VERTICAL_CENTER,
                      classe);
    MLV_actualise_window();
}

void effacer_classe_saisie()
{
    MLV_draw_filled_rectangle(b_classe->x + b_classe->width, b_classe->y,
                              b_classe->width, b_classe->height,
                              MLV_COLOR_BLACK);
    MLV_actualise_window();
}

/****
 * Les coordonnées du point à effacer.
 * (du dernier point cliqué)
 ****/
void afficher_coord_point_effacer(point p)
{
    MLV_draw_text_box(b_effacer->x + b_effacer->width, b_effacer->y,
                      b_effacer->width, b_effacer->height,
                      "x= %lf\ny= %lf",
                      3,
                      MLV_COLOR_BLACK,
                      MLV_COLOR_GREY,
                      MLV_COLOR_BLACK,
                      MLV_TEXT_CENTER,
                      MLV_HORIZONTAL_LEFT,
                      MLV_VERTICAL_CENTER,
                      p.x,
                      p.y);
    MLV_actualise_window();
}

void effacer_coord_point_effacer()
{
    MLV_draw_filled_rectangle(b_effacer->x + b_effacer->width, b_effacer->y,
                              b_effacer->width, b_effacer->height,
                              MLV_COLOR_BLACK);
    MLV_actualise_window();
}

/****
 * Le nom du fichier chargé.
 ****/
void afficher_nom_fichier_saisie(char *nom)
{
    MLV_draw_text_box(b_chargement->x,
                      b_chargement->y + b_chargement->height,
                      WIDTH - b_chargement->x,
                      b_chargement->height,
                      "%s",
                      3,
                      MLV_COLOR_BLACK,
                      MLV_COLOR_GREY,
                      MLV_COLOR_BLACK,
                      MLV_TEXT_CENTER,
                      MLV_HORIZONTAL_LEFT,
                      MLV_VERTICAL_CENTER,
                      nom);
    MLV_actualise_window();
}

void effacer_nom_fichier_saisie()
{
    MLV_draw_filled_rectangle(b_chargement->x,
                              b_chargement->y + b_chargement->height,
                              WIDTH - b_chargement->x,
                              b_chargement->height,
                              MLV_COLOR_BLACK);
    MLV_actualise_window();
}

/****
 * La valeur de k.
 ****/
void afficher_valeur_k_saisie(int k)
{
    MLV_draw_text_box(b_valeur_k->x + b_valeur_k->width,
                      b_valeur_k->y,
                      b_valeur_k->width * 2,
                      b_valeur_k->height,
                      "%3d",
                      3,
                      MLV_COLOR_BLACK,
                      MLV_COLOR_GREY,
                      MLV_COLOR_BLACK,
                      MLV_TEXT_CENTER,
                      MLV_HORIZONTAL_LEFT,
                      MLV_VERTICAL_CENTER,
                      k);
    MLV_actualise_window();
}

void effacer_valeur_k_saisie()
{
    MLV_draw_filled_rectangle(b_valeur_k->x + b_valeur_k->width, b_valeur_k->y,
                              b_valeur_k->width * 2, b_valeur_k->height,
                              MLV_COLOR_BLACK);
    MLV_actualise_window();
}
