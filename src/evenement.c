/********************************************************
 * Contient les fonctions permettant de traiter les     *
 * évènements du programme = clique sur les boutons.    *
 * Ce module a pour but d'aléger la fonction main, et   *
 * de rendre le traitement des évènements plus lissible *
 ********************************************************/
#include <MLV/MLV_all.h>
#include "../inc/affichage_des_menus.h" /* inclus bouton.h et type.h */
#include "../inc/afficher_resultat_input.h"
#include "../inc/allocation.h"
#include "../inc/arbre_kd.h"
#include "../inc/arbre_kd_graphique.h"
#include "../inc/cercle.h"
#include "../inc/coordonnees.h"
#include "../inc/dessiner_plan.h"
#include "../inc/dessiner_point.h"
#include "../inc/fichier.h"
#include "../inc/k_ppv.h" /* inclus liste.h */

#define NB_POINTS 50           /* pour définir le nbr de case allouées au tableau pour le mode création */
#define NB_POINTS_REALLOUER 25 /* défini le nbr de points réallouer si le tableau est plein */

/****
 * Traite les évènements spécifiques du mode création.
 ****/
void event_mode_creation(int x, int y, tableau_de_points *t, int *compteur, arbre_kd *a)
{
  char *input_text = NULL; /* pour les boites de saisie */
  char *chemin, *nom;
  int classe, n = t->nb_points - 1;

  /* ***** ON AJOUTE UN POINT ***** */
  if (est_clique(x, y, b_plan))
  {
    if (t->tab == NULL)
    {
      /*--il faut allouer le tableau--*/
      t->tab = (point *)allocation_mem(NB_POINTS, sizeof(point));
      t->taille = NB_POINTS;
      t->nb_points = 0;
    }
    else if (t->nb_points == t->taille)
    {
      /*--tab plein, il faut l'agrandir--*/
      reallocation_mem((void **)&(t->tab), t->taille + NB_POINTS_REALLOUER, sizeof(point));
      t->taille += NB_POINTS_REALLOUER;
    }
    /*--ajout du point au tableau, et mise à jour des compteurs--*/
    n++;
    t->tab[n] = affiche_point_clique(x, y);
    (t->nb_points)++;
    (*compteur)++;

    /*--on affiche les coordonnées du point cliqué à coté du bouton effacer--*/
    afficher_coord_point_effacer(t->tab[n]);

    /*--on oblige l'utilisateur à rentrer une classe--*/
    x = b_classe->x + 1;
    y = b_classe->y + 1;
  }

  /* ***** ON DEMANDE LA CLASSE DU DERNIER POINT AJOUTE ***** */
  if (est_clique(x, y, b_classe))
  {
    /*--on vérifie d'abord que le tableau ne soit pas vide et que l'on a ajouter un point manuellement--*/
    if (t->nb_points != 0 && *compteur != 0)
    {
      do
      {
        input_text = demande_valeur(b_classe);
        classe = atoi(input_text);
        /*--test si la valeur est correcte--*/
        if (classe <= 0 || classe > NB_CLASSES)
        {
          fprintf(stderr, "Erreur de saisie pour la classe.\n");
        }
        /*--si oui, alors on l'affiche et change la classe du point--*/
        else
        {
          afficher_classe_saisie(classe);
          affiche_point_classe(t->tab[n], EFFACE);
          t->tab[n].classe = classe;
          affiche_point_classe(t->tab[n], AFFICHE);
        }
        free(input_text);
        /*si le point n'avais pas encore de classe (0) et la classe entrée est
    incorrecte, alors il faut redemander une classe */
      } while (t->tab[n].classe == 0);
    }
  }

  /* ***** ON EFFACE LE DERNIER POINT CLIQUE ***** */
  else if (est_clique(x, y, b_effacer) && *compteur > 0)
  {
    if (t->nb_points != 0)
    {
      (t->nb_points)--;
      (*compteur)--;
      affiche_point_classe(t->tab[n], EFFACE);
      /*--on change l'affichage des coordonnées du point qui peut être effacé--*/
      if (*compteur != 0)
      {
        afficher_coord_point_effacer(t->tab[n - 1]);
      }
      else
      {
        effacer_coord_point_effacer();
      }
    }
  }

  /* ***** ON CHARGE UN FICHIER DE DONNEES ***** */
  else if (est_clique(x, y, b_chargement))
  {
    chemin = demande_valeur(b_chargement);
    if (!MLV_path_exists(chemin) || !MLV_path_is_a_file(chemin))
    {
      fprintf(stderr, "Erreur le chemin '%s' est invalide.\n", chemin);
      afficher_nom_fichier_saisie("fichier inexistant");
    }
    else
    {
      /*--sinon on affiche le nom du fichier--*/
      nom = MLV_get_base_name(chemin);
      afficher_nom_fichier_saisie(nom);
      free(nom);

      if (t->nb_points != 0)
      {
        /*on libère l'espace mémoire tu tableau de point */
        free(t->tab);
        t->tab = NULL;
        t->taille = 0;
        /*on efface tout le plan*/
        afficher_plan_vide();
      }
      if (!est_vide_arbre_kd(*a))
      {
        libere_mem_arbre(*a);
        (*a) = arbre_kd_vide();
      }
      *compteur = 0;
      /*--on charge et affiche les points--*/
      chargement_fichier(chemin, t, a);
      afficher_tableau_de_points(*t);
    }
    free(chemin);
  }

  /* ***** ON SAUVEGARDE LE JEU DE DONNEES CREE ***** */
  else if (est_clique(x, y, b_sauvegarde))
  {
    /*on vérifie que l'on a ajouté des points manuellement*/
    if ((*compteur) > 0)
    {
      /*lancement de la fct sauvegarde dans un fichier*/
      chemin = demande_valeur(b_chargement);
      if (MLV_path_exists(chemin))
      {
        fprintf(stderr, "Erreur le chemin '%s' est existant, sauvegarde non effectuée.\n", chemin);
        afficher_nom_fichier_saisie("fichier existant");
      }
      else
      {
        /*--sinon on affiche le nom du fichier et on sauvegarde--*/
        nom = MLV_get_base_name(chemin);
        afficher_nom_fichier_saisie(nom);
        free(nom);
        sauvegarde_fichier(*t, chemin);
        free(chemin);
      }
    }
  }

  /* ***** ON PASSE EN MODE KPPV ***** */
  else if (est_clique(x, y, b_KPPV) && t->nb_points != 0)
  {
    change_mode(b_KPPV, b_creation);
    /*--si on a rajouté des points manuellement, on les rajoute dans l'arbre--*/
    while (*compteur != 0)
    {
      (*a) = inserer_point_arbre_kd(*a, &(t->tab[n]));
      n--;
      (*compteur)--;
    }
    /*--par défaut la structure sera un tableau--*/
    change_structure(b_tableau, b_arbre);
  }
}

/****
 * Actualise l'affichage, après que des modifications d'options on été faites.
 ****/
typedef enum
{
  K_MODIFIE,
  CERCLE_VOISINAGE,
  RAFRAICHIR,
  SUPPRIME_POINT,
  CHANGE_STRUCTURE
} event;

static void actualiser_affichage_kppv(int old_k, int k, point *p_clique, liste_KPPV *l, tableau_de_points t, arbre_kd a, event event)
{
  int i;
  static liste_KPPV l2; /* conserve le pointeur sur le k° voisin comme ça si on rappelle
         la fonction avec un k>old_k on ne reparcours pas les old_k premiers de la liste */

  if (event == K_MODIFIE)
  {
    /* ***** on regarde s'il y a des options d'affichages à effacer ***** */
    if (b_voisinage->etat == ACTIF)
    {
      /*--on supprime le cercle--*/
      if (!est_liste_vide(l2))
      {
        dessiner_cercle(*p_clique, *(l2->point), MLV_COLOR_BLACK);
      }
    }
    if (b_decision->etat == ACTIF)
    {
      affiche_point_classe(*p_clique, EFFACE);
    }

    /* ***** on change l'affichege des voisins suivant la structure utilisée ***** */
    if (b_tableau->etat == ACTIF)
    {
      if (old_k == 0)
      {
        /*--on met en valeur les k-ppv--*/
        l2 = mise_en_avant_k_ppv(k, (*l));
      }
      else if (k > old_k)
      {
        /*--on met en valeur les voisins qui ne l'étaient pas--*/
        l2 = mise_en_avant_k_ppv(k - old_k, l2->suivant);
      }
      else if (k < old_k)
      {
        /*--il faut 'supprimer' la mise en avant des points de k à old_k--*/
        l2 = (*l);
        for (i = 1; i < k; i++)
        {
          l2 = l2->suivant;
        }
        supprime_mise_en_avant(old_k - k, l2->suivant);
      }
    }
    else
    {
      /*--on utilise l'arbre--*/
      if (!est_liste_vide(*l))
      {
        supprime_mise_en_avant(old_k, *l);
        detruire_liste(l);
      }
      l2 = rechercher(a, p_clique, k);
      (*l) = l2;
      mise_en_avant_k_ppv(k, *l);
    }

    /* ***** on regarde s'il y a des options d'affichages à actualiser ***** */
    if (b_voisinage->etat == ACTIF)
    {
      dessiner_cercle(*p_clique, *(l2->point), MLV_COLOR_RED);
    }
    if (b_decision->etat == ACTIF)
    {
      /*--on attribut une classe au point clique--*/
      kppv_decision(*l, p_clique, k);
      affiche_point_classe(*p_clique, AFFICHE);
    }
  }

  else if (event == CERCLE_VOISINAGE)
  {
    if (b_voisinage->etat == ACTIF)
    {
      dessiner_cercle(*p_clique, *(l2->point), MLV_COLOR_RED);
    }
    else
    {
      dessiner_cercle(*p_clique, *(l2->point), MLV_COLOR_BLACK);
    }
  }

  else if (event == SUPPRIME_POINT)
  {
    /*--on efface les coord du point, k, le plan et réaffiche les points--*/
    effacer_valeur_k_saisie();
    effacer_coord_point_effacer();
    afficher_plan_vide();
    afficher_tableau_de_points(t);
    l2 = (*l);
  }

  else if (event == CHANGE_STRUCTURE)
  {
    /*--on réinitialise le plan--*/
    afficher_plan_vide();
    afficher_tableau_de_points(t);
    affiche_point_classe(*p_clique, EN_VALEUR);
    /*--on crée la liste de voisin et les mets en valeurs--*/
    if (b_tableau->etat == ACTIF)
    {
      (*l) = creer_liste_voisins(t, *p_clique);
      l2 = mise_en_avant_k_ppv(k, *l);
    }
    else
    {
      l2 = rechercher(a, p_clique, k);
      (*l) = l2;
      mise_en_avant_k_ppv(k, *l);
      if (b_decoupe_plan->etat == ACTIF)
      {
        decoupe_plan(a, MLV_COLOR_WHITE);
      }
    }
    /*--options d'affichage--*/
    if (b_voisinage->etat == ACTIF)
    {
      dessiner_cercle(*p_clique, *(l2->point), MLV_COLOR_RED);
    }
    if (b_decision->etat == ACTIF)
    {
      kppv_decision(*l, p_clique, k);
      affiche_point_classe(*p_clique, AFFICHE);
    }
  }
}

/* ***
 * demande et récupère la valeur de k saisie par l'utilisateur
 * renvoie 1 si k est modifié 0 sinon
 *** */
static int demande_valeur_de_k(int *k, int max)
{
  char *input_text;
  int k2, ok = 1;

  do
  {
    input_text = demande_valeur(b_valeur_k);
    k2 = atoi(input_text);
    free(input_text);

    if (k2 <= 0 || k2 > max)
    {
      fprintf(stderr, "Erreur de saisie pour k.\n");
      ok = 0;
    }
    else
    {
      (*k) = k2;
    }
  } while (*k == 0); /*oblige l'utilisateur à rentrer une valeur correcte la 1° fois*/

  return ok;
}

/****
 * Taite les évènements en mode KPPV
 ****/
void event_mode_KPPV(int x, int y, int *k, point *p_clique, tableau_de_points t, liste_KPPV *l, arbre_kd a)
{
  int old_k = (*k);

  /* ***** ON CHANGE DE STRUCTURE ***** */
  if (b_arbre->etat == ACTIF && est_clique(x, y, b_tableau))
  {
    change_structure(b_tableau, b_arbre);
    detruire_liste(l);
    if (*k != 0)
    {
      actualiser_affichage_kppv(old_k, *k, p_clique, l, t, a, CHANGE_STRUCTURE);
    }
  }
  else if (b_tableau->etat == ACTIF && est_clique(x, y, b_arbre))
  {
    change_structure(b_arbre, b_tableau);
    detruire_liste(l);
    if (*k != 0)
    {
      actualiser_affichage_kppv(old_k, *k, p_clique, l, t, a, CHANGE_STRUCTURE);
    }
  }

  /*--si un point à été cliqué--*/
  else if (*k != 0)
  {
    /* ***** ON CHANGE LA VALEUR DE K ***** */
    if (est_clique(x, y, b_valeur_k))
    {
      if (demande_valeur_de_k(k, t.nb_points))
      {
        /*--si k a été modifié, on actualise l'affichage--*/
        afficher_valeur_k_saisie(*k);
        actualiser_affichage_kppv(old_k, *k, p_clique, l, t, a, K_MODIFIE);
      }
    }

    /* ***** ON AFFICHE/EFFACE LE CERCLE DE VOISINAGE ***** */
    else if (est_clique(x, y, b_voisinage))
    {
      if (b_voisinage->etat == ACTIF)
      {
        /*--on désactive le bouton et efface le cercle--*/
        b_voisinage->etat = VISIBLE;
        actualiser_affichage_kppv(old_k, *k, p_clique, l, t, a, CERCLE_VOISINAGE);
      }
      else
      {
        /*--on l'active et affiche le cercle--*/
        b_voisinage->etat = ACTIF;
        actualiser_affichage_kppv(old_k, *k, p_clique, l, t, a, CERCLE_VOISINAGE);
      }
      afficher_bouton(b_voisinage);
    }

    /* ***** ON CHANGE L'AFFICHAGE DU POINT CLIQUE ***** */
    else if (est_clique(x, y, b_decision))
    {
      /*--on efface le point cliqué--*/
      affiche_point_classe(*p_clique, EFFACE);

      if (b_decision->etat == ACTIF)
      {
        /*--on le désactive et supprime la classe du point donnée--*/
        b_decision->etat = VISIBLE;
        p_clique->classe = 0;
      }
      else
      {
        /*--on l'active et donne une classe au point--*/
        b_decision->etat = ACTIF;
        kppv_decision(*l, p_clique, *k);
      }

      /*--on affiche les changements effectués--*/
      affiche_point_classe(*p_clique, AFFICHE);
      afficher_bouton(b_decision);
    }

    /* ***** ON EFFACE LE POINT ET LA LISTE DES KPPV ***** */
    else if (est_clique(x, y, b_effacer))
    {
      /*--on supprime la liste des KPPV, et  la valeur de k puis,
        on met à jour l'affichage--*/
      detruire_liste(l);
      (*k) = 0;
      effacer_valeur_k_saisie();
      actualiser_affichage_kppv(old_k, *k, p_clique, l, t, a, SUPPRIME_POINT);
    }
  }

  /* ***** SINON AUCUN POINT CLIQUE ***** */

  /* ***** ON A CLIQUE SUR LE PLAN ***** */
  else if (est_clique(x, y, b_plan))
  {
    /*--on affiche le point clique et ces coordonnées--*/
    (*p_clique) = affiche_point_clique(x, y);
    afficher_coord_point_effacer(*p_clique);

    /*--on demande la valeur de k--*/
    demande_valeur_de_k(k, t.nb_points);
    afficher_valeur_k_saisie(*k);

    /*--on crée la liste des voisins pour le tableau--*/
    if (b_tableau->etat == ACTIF)
    {
      (*l) = creer_liste_voisins(t, *p_clique);
    }

    /*--on actualise l'affichage--*/
    actualiser_affichage_kppv(old_k, *k, p_clique, l, t, a, K_MODIFIE);
  }

  /* ***** ON PASSE EN MODE CREATION ***** */
  else if (est_clique(x, y, b_creation))
  {
    change_mode(b_creation, b_KPPV);
  }

  /* ***** ON DECOUPE LE PLAN EN ZONES ***** */
  if (est_clique(x, y, b_decoupe_plan) && b_arbre->etat == ACTIF)
  {
    if (b_decoupe_plan->etat == VISIBLE)
    {
      b_decoupe_plan->etat = ACTIF;
      decoupe_plan(a, MLV_COLOR_WHITE);
    }
    else
    {
      b_decoupe_plan->etat = VISIBLE;
      decoupe_plan(a, MLV_COLOR_BLACK);
    }
    afficher_bouton(b_decoupe_plan);
  }

  /* ***** ANIMATION DE RECHERCHE DANS L'ARBRE ***** */
  else if (est_clique(x, y, b_animation) && b_arbre->etat == ACTIF)
  {
    if (b_animation->etat == VISIBLE)
    {
      b_animation->etat = ACTIF;
    }
    else
    {
      b_animation->etat = VISIBLE;
    }
    afficher_bouton(b_animation);
  }
}
