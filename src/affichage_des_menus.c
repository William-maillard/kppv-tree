/******************************************************
 * Définitions des fonctions pour :                   *
 *   -attribuer les dimensions à chaque bouton        *
 *   -gérer l'affichage des options pour chaque mode  *
 *   -afficher une boite de saisie sur un bouton      *
 ******************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MLV/MLV_all.h>
#include "../inc/affichage_des_menus.h"
#include "../inc/afficher_resultat_input.h"
#include "../inc/dessiner_plan.h"

/****
 * affiche/efface le cadre et le texte "options affichage"
 ****/
void afficher_cadre_options_affichage(MLV_Color c)
{
  MLV_draw_rectangle(WIDTH - 217, 320, 217, 142, c);

  MLV_draw_text(WIDTH - 217, 325, "     OPTIONS D'AFFICHAGE", c);
  MLV_actualise_window();
}

/* ***
 * affiche/efface le cadre et le texte "structure de donnee"
 *** */
void afficher_cadre_structure_de_donnee(MLV_Color c)
{
  MLV_draw_rectangle(WIDTH - 210,
                     170,
                     210,
                     100,
                     c);
  MLV_draw_text(WIDTH - 210,
                175,
                "    STRUCTURE DE DONNEE",
                c);
  MLV_actualise_window();
}

/****
 * affiche et affecte les dimensions des boutons déclarés dans bouton.h,
 * il doivent être créé avant
 *** */
void afficher_les_boutons_clickables()
{
  int interligne = 4; /* distance entre le texte et les bords */

  /* ***********
   *  -On affecte aux boutons leurs coordonnées (x,y) Nord-Ouest et
   *  leurs dimensions à leurs champs respectifs,
   *  -on affiche le mode création.
   * ********** */

  /* ***** BOUTON CREATION ***** */

  MLV_get_size_of_adapted_text_box(b_creation->name,
                                   interligne,
                                   &(b_creation->width),
                                   &(b_creation->height));

  b_creation->x = DROITE_PLAN + PADDING + 2; /* sur le cote droit du plan */
  b_creation->y = HAUT_PLAN - PADDING;       /* aligne avec le haut du cadre */

  /* ***** BOUTON K-PPV ***** */

  b_KPPV->x = b_creation->x + b_creation->width + 5; /*a cote du bouton creation */
  b_KPPV->y = b_creation->y;

  MLV_get_size_of_adapted_text_box(b_KPPV->name,
                                   interligne,
                                   &(b_KPPV->width),
                                   &(b_KPPV->height));

  /* ***** BOUTON VALEUR DE K ***** */

  b_valeur_k->x = b_KPPV->x + b_KPPV->width + 5; /*a cote du bouton KPPV */
  b_valeur_k->y = b_KPPV->y;

  MLV_get_size_of_adapted_text_box(b_valeur_k->name,
                                   interligne,
                                   &(b_valeur_k->width),
                                   &(b_valeur_k->height));

  /* ***** BOUTON QUITTER ***** */

  MLV_get_size_of_adapted_text_box(b_quitter->name,
                                   interligne,
                                   &(b_quitter->width),
                                   &(b_quitter->height));

  /*--coller au bord en haut à droite--*/
  b_quitter->x = WIDTH - b_quitter->width;
  b_quitter->y = 0;

  b_quitter->etat = QUITTER;
  afficher_bouton(b_quitter);

  /* ***** BOUTON REINITIALISATION DE LA FENETRE ***** */

  MLV_get_size_of_adapted_text_box(b_reinitialisation->name,
                                   interligne,
                                   &(b_reinitialisation->width),
                                   &(b_reinitialisation->height));

  /*--coller au bord en haut à droite en dessous de quitter--*/
  b_reinitialisation->x = WIDTH - b_reinitialisation->width;
  b_reinitialisation->y = b_quitter->height + 2;

  b_reinitialisation->etat = VISIBLE;
  afficher_bouton(b_reinitialisation);

  /* ***** BOUTON EFFACER ***** */

  MLV_get_size_of_adapted_text_box(b_effacer->name,
                                   interligne,
                                   &(b_effacer->width),
                                   &(b_effacer->height));

  /*--en dessous du bouton creation--*/
  b_effacer->x = DROITE_PLAN + PADDING + 2;
  b_effacer->y = b_creation->y + b_creation->height + 20;

  b_effacer->etat = VISIBLE;
  afficher_bouton(b_effacer);

  /* ***** BOUTON CLASSE ***** */

  MLV_get_size_of_adapted_text_box(b_classe->name,
                                   interligne,
                                   &(b_classe->width),
                                   &(b_classe->height));

  /*--en dessous du bouton effacer--*/
  b_classe->x = DROITE_PLAN + PADDING + 2;
  b_classe->y = b_effacer->y + b_effacer->height + 5;

  /* ***** OPTIONS D'AFFICHAGE ***** */

  /* ** VOISINAGE / AVEC PRISE DE DECISION  ** */
  /* ** DECOUPE_PLAN / ANIMATION ** */

  MLV_get_size_of_adapted_text_box(b_voisinage->name,
                                   interligne,
                                   &(b_voisinage->width),
                                   &(b_voisinage->height));

  MLV_get_size_of_adapted_text_box(b_decision->name,
                                   interligne,
                                   &(b_decision->width),
                                   &(b_decision->height));

  MLV_get_size_of_adapted_text_box(b_decoupe_plan->name,
                                   interligne,
                                   &(b_decoupe_plan->width),
                                   &(b_decoupe_plan->height));

  MLV_get_size_of_adapted_text_box(b_animation->name,
                                   interligne,
                                   &(b_animation->width),
                                   &(b_animation->height));

  /*--coller au bord droit, environ milieu vertical
    les uns en dessous des autres--*/
  b_voisinage->x = WIDTH - 40 - b_decision->width;
  b_voisinage->y = HEIGHT / 2;
  b_decision->x = b_voisinage->x;
  b_decision->y = b_voisinage->y + b_voisinage->height + 2;
  b_decoupe_plan->x = b_voisinage->x;
  b_decoupe_plan->y = b_decision->y + b_decision->height + 2;
  b_animation->x = b_voisinage->x;
  b_animation->y = b_decoupe_plan->y + b_decoupe_plan->height + 2;

  /* ***** OPTIONS STRUCTURE DE DONNEE ***** */

  /* ** TABLEAU / ARBRE ***** */
  MLV_get_size_of_adapted_text_box(b_tableau->name,
                                   interligne,
                                   &(b_tableau->width),
                                   &(b_tableau->height));

  MLV_get_size_of_adapted_text_box(b_arbre->name,
                                   interligne,
                                   &(b_arbre->width),
                                   &(b_arbre->height));

  /*--au dessus du cardre 'option affichage' */
  b_tableau->x = WIDTH - b_tableau->width - 80;
  b_tableau->y = b_voisinage->y - b_tableau->height - 100;
  b_arbre->x = b_tableau->x + 5;
  b_arbre->y = b_tableau->y - b_arbre->height - 4;

  /* ***** BOUTON SAUVEGARDE DES DONNEES ENTREES ***** */

  MLV_get_size_of_adapted_text_box(b_sauvegarde->name,
                                   interligne,
                                   &(b_sauvegarde->width),
                                   &(b_sauvegarde->height));

  /*--en bas à droite dans le coin--*/
  b_sauvegarde->x = WIDTH - b_sauvegarde->width;
  b_sauvegarde->y = HEIGHT - b_sauvegarde->height;

  /* ***** BOUTON CHARGEMENT D'UN FICHIER ***** */

  MLV_get_size_of_adapted_text_box(b_chargement->name,
                                   interligne,
                                   &(b_chargement->width),
                                   &(b_chargement->height));

  /*--au dessus du bouton sauvegarde--*/
  b_chargement->x = b_sauvegarde->x;
  b_chargement->y = b_sauvegarde->y - 2 * b_chargement->height - 5;

  /* ***** AFFICHAGE ***** */
  change_mode(b_creation, b_KPPV);
}

/* *** Permet de passer du mode 'current_mode' au mode 'new_mode' *** */
void change_mode(bouton new_mode, bouton current_mode)
{
  MLV_Color couleur; /*pour les cadres*/

  /*-- activation/désactivation + changement d'affichage des modes --*/
  current_mode->etat = VISIBLE;
  new_mode->etat = ACTIF;
  afficher_bouton(current_mode);
  afficher_bouton(new_mode);

  /*-- Suivant le mode actif,
    on affiche/efface les boutons spécifique au mode--*/
  if (b_creation->etat == ACTIF)
  {
    /* ***** MODE CREATION ***** */
    /*--on rend visible les boutons du mode création--*/
    b_classe->etat = VISIBLE;
    b_chargement->etat = VISIBLE;
    b_sauvegarde->etat = VISIBLE;

    /*--on rend invisible les boutons du mode KPPV--*/
    b_voisinage->etat = INVISIBLE;
    b_decision->etat = INVISIBLE;
    b_decoupe_plan->etat = INVISIBLE;
    b_animation->etat = INVISIBLE;

    b_valeur_k->etat = INVISIBLE;
    b_tableau->etat = INVISIBLE;
    b_arbre->etat = INVISIBLE;

    /* on efface les cadres */
    couleur = MLV_COLOR_BLACK;
  }

  else
  {
    /* ***** MODE KPPV ***** */
    /*--on rend visible les boutons du mode KPPV--*/
    b_voisinage->etat = VISIBLE;
    b_decision->etat = VISIBLE;
    b_decoupe_plan->etat = VISIBLE;
    b_animation->etat = VISIBLE;

    b_valeur_k->etat = VISIBLE;
    b_tableau->etat = ACTIF;
    b_arbre->etat = VISIBLE;

    /*--on rend visible les cadres--*/
    couleur = MLV_COLOR_WHITE;

    /*--on rend invisible les boutons du mode création--*/
    b_classe->etat = INVISIBLE;
    b_chargement->etat = INVISIBLE;
    b_sauvegarde->etat = INVISIBLE;

    /* on efface les valeurs affichées*/
    effacer_classe_saisie();
    effacer_nom_fichier_saisie();
    effacer_coord_point_effacer();
  }
  /*--on affiche les changements--*/
  afficher_bouton(b_sauvegarde);
  afficher_bouton(b_chargement);
  afficher_bouton(b_classe);
  afficher_bouton(b_voisinage);
  afficher_bouton(b_decision);
  afficher_bouton(b_decoupe_plan);
  afficher_bouton(b_animation);
  afficher_bouton(b_valeur_k);
  afficher_bouton(b_tableau);
  afficher_bouton(b_arbre);

  afficher_cadre_options_affichage(couleur);
  afficher_cadre_structure_de_donnee(couleur);
}

/* ***
 * Change la structure utilisée pour la recherche des k-ppv.
 *** */
void change_structure(bouton new_structure, bouton current_structure)
{
  /*--changement de la structure active--*/
  current_structure->etat = VISIBLE;
  new_structure->etat = ACTIF;

  /*--activation/désactivation des options d'affichage spécifique --*/
  if (b_tableau->etat == ACTIF)
  {
    b_decoupe_plan->etat = INVISIBLE;
    b_animation->etat = INVISIBLE;
  }
  else
  {
    b_decoupe_plan->etat = VISIBLE;
    b_animation->etat = VISIBLE;
  }

  /*--affichage des changements--*/
  afficher_bouton(current_structure);
  afficher_bouton(new_structure);
  afficher_bouton(b_decoupe_plan);
  afficher_bouton(b_animation);
}

/****
 *  Affiche une input box sur le bouton en paramètre
 *  et renvoie le text rentré par l'utilisateur.
 ****/
char *demande_valeur(bouton b)
{
  char *s;

  MLV_wait_input_box(b->x, b->y,
                     (int)b->width * 1.5, b->height,
                     MLV_COLOR_RED, MLV_COLOR_WHITE,
                     MLV_COLOR_BLACK, b->name,
                     &s);

  return s;
}
