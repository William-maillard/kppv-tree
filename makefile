CC= gcc
CFLAGS= -W -Wall -std=c89 -pedantic -O3 `pkg-config --cflags MLV` `pkg-config --libs-only-other --libs-only-L MLV`
LDLIBS=`pkg-config --libs-only-l MLV`

OBJETS= obj/affichage_des_menus.o \
		obj/allocation.o \
		obj/arbre_kd.o \
		obj/arbre_kd_graphique.o \
		obj/bouton.o  \
		obj/cercle.o \
		obj/coordonnees.o \
		obj/dessiner_plan.o \
		obj/dessiner_point.o \
		obj/evenement.o \
		obj/afficher_resultat_input.o \
		obj/fichier.o \
		obj/k_ppv.o \
		obj/listes.o  \
		obj/main.o

all: main gen_data

main: $(OBJETS)
	$(CC) $(CFLAGS) $(OBJETS) $(LDLIBS) -o bin/main

gen_data: obj/gen_data.o
	$(CC) $(OPTIONS) $^ -o bin/gen_data

obj/%.o : src/%.c
	$(CC) $(CFLAGS) -c $^ $(LDLIBS) -o obj/$*.o

clean:
	rm -f obj/* bin/*

clean_data:
	rm -f data/*
