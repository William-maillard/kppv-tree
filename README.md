# kppv-tree

Représentation graphique de la recherche du k-ppv an utilisant un arbre.

## Description

PROJET D'IMPLÉMENTATION DES K PLUS PROCHES VOISINS avec prise de décision
en utilisant un tableau, puis un arbre qui découpe l'espace

créer l' exécutable: "make" <br />
prog de gen de fichier bin/gen_data <br />
prog principale : bin/main<br />
Suppression des .o et de l'exécutable : make clean

## Illustations de l'application

### mode création

![Home](/doc/screen-shots/home.png)
![création point](/doc/screen-shots/clique.png)
![création point](/doc/screen-shots/apres_clique.png)
![chargement](/doc/screen-shots/load_file.png)
![chargement](/doc/screen-shots/)

### mode kppv

![Home](/doc/screen-shots/30nn.png)
![Arbre](/doc/screen-shots/arbre_decoupe_plan.png)
![Arbre](/doc/screen-shots/arbre_decoupe_plan_decision.png)

#### Animation

![Arbre](/doc/screen-shots/knn_tree_animation.gif)

Explications couleurs :

- vert: exploration d'une branche
- bleu: retour dans la branche parent après fin de l'exploration de la branche courante

On peut voir que grace à cette méthode on ne parcours pas tous les points mais juste ceux qui
se situent proche du point.
